﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour {

	public bool isMe=false;

	public int playerID = 0;

	float screenedgex=0f;
	float screenedgey=0f;

	public DBController gameController;

	// Use this for initialization
	void Start () {
		screenedgex = Camera.main.aspect*Camera.main.orthographicSize;
		screenedgey = Camera.main.orthographicSize;
		
	}

	//control when the player starts synchronizing
	public void startSync()
	{
		if (playerID != 0)
		{	
			StartCoroutine(gameController.syncPosition(this.gameObject));
		}
	}


	void leaveGame()
	{
		
		StartCoroutine(gameController.leaveGame(playerID));
	}



	
	// Update is called once per frame
	void Update () {
		if (isMe){
			transform.Translate(Vector3.right * 15f * Input.GetAxis("Horizontal")*Time.deltaTime);
			transform.Translate(Vector3.up * 15f * Input.GetAxis("Vertical")*Time.deltaTime);
			transform.position = new Vector3(
				Mathf.Clamp(transform.position.x,-screenedgex,screenedgex),
				Mathf.Clamp(transform.position.y,-screenedgey,screenedgey));
		}

		if (Input.GetKeyDown(KeyCode.Escape))
		{
			//if I press escape, trigger leave game
			leaveGame();
		}
	}
}
