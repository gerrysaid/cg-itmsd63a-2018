﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
//-------------------------------------
using UnityEngine.SceneManagement;

public class DBController : MonoBehaviour {

	Text outputText;
	//the URL refers to the url of the web application that we are connecting to
	string url = "http://localhost:8084/example1/index.php";
	//string url = "http://M201-T:8084/example1/index.php";

	string nickname;

	bool joingame = false;

	bool gamerunning = false;

	//the list of all the players who are currently in the game
	public List<GameObject> currentPlayers;

	GameObject player,currentPlayer;
	// Use this for initialization


	void joinbuttonclicked()
	{
		nickname = GameObject.Find("NicknameField").GetComponent<InputField>().text;
		joingame = true;
		SceneManager.LoadScene("main");
		
	}

	
	void OnEnable(){
		SceneManager.sceneLoaded += OnSceneLoaded;
	}


	void OnSceneLoaded(Scene scene,LoadSceneMode mode)
	{
		if (scene.name=="main")
		{
				player = Resources.Load<GameObject>("Player");
			//create a new instance of my player IN THE CLIENT
				currentPlayer = Instantiate(player,
								new Vector3(0f,0f),
								Quaternion.identity);
				
				currentPlayer.name = nickname;

				currentPlayer.GetComponent<SpriteRenderer>().color = Random.ColorHSV();

				//set the name of each player box
				currentPlayer.GetComponentInChildren<TextMesh>().text = currentPlayer.name;

				currentPlayer.AddComponent<playerController>();

				currentPlayer.GetComponent<playerController>().isMe=true;

			//insert 'me' into the database IN THE SERVER



			initgame();
		}
	}

	IEnumerator joinGameOnServer()
	{
		string querystring = "?operation=join";
		string fullUrl = url+querystring;

		string nicknamestring = "&nickname="+nickname;
		fullUrl += nicknamestring;

		Color playerColour = currentPlayer.GetComponent<SpriteRenderer>().color;

		string colourstring = "&r="+playerColour.r+"&g="+playerColour.g+"&b="+playerColour.b;
		fullUrl += colourstring;

		Debug.Log(fullUrl);

		//like this, we have inserted the colour into the database
		//http://localhost:8080/example1/index.php?operation=join&nickname=gerry&r=0.4&g=0.34&b=0.1

		WWW webresult = new WWW(fullUrl);

		//wait for the result of the web query
		yield return webresult;
		
		//the raw text of the PHP returned
		Debug.Log(webresult.text);

		string[] fields = webresult.text.Split('|');

		if (fields[0]=="OK")
		{
			Debug.Log("Successfully joined game");
			Debug.Log(fields[1]);
			currentPlayer.GetComponent<playerController>().playerID = int.Parse(fields[1]);
			currentPlayer.GetComponent<playerController>().gameController = this;
			currentPlayer.GetComponent<playerController>().startSync();
			gamerunning = true;
			//now the game is running therefore gamerunning is true
			StartCoroutine(getPlayers(gamerunning));
			StartCoroutine(getPlayerPositions());
		}
		else if (fields[0]=="NOK")
		{
			Debug.Log("Too many players");
			outputText.text="Cannot join, too many players!, please try again later";
			yield return new WaitForSeconds(1f);
			SceneManager.LoadScene("join");
		}

		
	}

	public IEnumerator leaveGame(int playerID)
	{
		string querystring = "?operation=leave";
		string fullurl = url+querystring;
		string playeridstring = "&playerid="+playerID;

		fullurl+=playeridstring;

		Debug.Log(fullurl);

		WWW webresult = new WWW(fullurl);
		
		yield return webresult;
		
		Debug.Log(webresult.text);

		string[] fields = webresult.text.Split('|');

		if (fields[0]=="OK")
		{
			Debug.Log("Successfully left game");
			
		}

		SceneManager.LoadScene("join");

	}

	
	public IEnumerator syncPosition(GameObject myPlayer)
	{
		while (true)
		{

			//http://localhost:8084/example1/index.php?operation=move&playerid=16&xpos=3&ypos=5
			string querystring = "?operation=move";
			string fullurl = url+querystring;
			string playeridstring = "&playerid="+myPlayer.GetComponent<playerController>().playerID;
			string xposstring = "&xpos="+myPlayer.transform.position.x;
			string yposstring = "&ypos="+myPlayer.transform.position.y;
			fullurl += playeridstring + xposstring + yposstring;
			//should look like the above
			//Debug.Log(fullurl);
			WWW webresult = new WWW(fullurl);
			yield return webresult;
			Debug.Log(webresult.text);
		}
	}


	public IEnumerator getPlayerPositions()
	{
		//update the positions for all the players in the game
		string querystring = "?operation=getposition";
		string fullurl = url+querystring;
		
		float xpos=0f,ypos = 0f;
		
		while (true)
		{
			//all the players who are not me
			foreach(GameObject player in currentPlayers.ToList()){
				//get the ID from the player
				int id = player.GetComponent<nonplayerController>().playerID;
				//http://localhost:8084/example1/index.php?operation=getposition&playerid=16
				
				fullurl = url+querystring;
				
				fullurl += "&playerid="+id;

				//write the url to the console
				Debug.Log(fullurl);

				WWW webresult = new WWW(fullurl);

				//wait for the result of the web query
				yield return webresult;

				string[] records = webresult.text.Split('%');

				foreach (string record in records){
					try{
						string[] fields = record.Split('|');
						xpos = float.Parse(fields[0]);
						ypos = float.Parse(fields[1]);
						player.transform.position = new Vector3(xpos,ypos);
						Debug.Log(id+" "+xpos+" "+ypos);	

					} catch(System.Exception e){

					}
				}
				//update the position of the player according to the position set in the server
				yield return null;
			}
			yield return null;
		}
	}
	


	void initgame()
	{
			player = Resources.Load<GameObject>("Player");

			//get the text component of nameslist
			outputText = GameObject.Find("NamesList").GetComponent<Text>();

			outputText.text="";
			//the data needs to come to the text field asynchronously
			StartCoroutine(getPlayers(gamerunning));
	}

	
	void Start () {
		//keep this object loaded all the time
		//---------------------------
		DontDestroyOnLoad(this);

		//initialize the list of players to an empty list
		currentPlayers = new List<GameObject>();

		if (SceneManager.GetActiveScene().name == "join"){
			Button joinGameButton = GameObject.Find("JoinButton").GetComponent<Button>();

			joinGameButton.onClick.AddListener(() => joinbuttonclicked());
		}



		//----------------------------------------
		//only do this if I am in the main scene
		if (SceneManager.GetActiveScene().name == "main"){
			//load the player prefab from the file in the resources folder
			Debug.Log("test");
		}
		//-------------------------------------------------
	}



	bool canAdd=true;

	//get a list of players from the server
	IEnumerator getPlayers(bool running)
	{
		string querystring = "?operation=select";		
		string fullURL = url+querystring;
		while (true) {		
			//http://localhost:8084/example1/index.php?operation=SELECT
			Debug.Log(fullURL);
			WWW webresult = new WWW(fullURL);
			//wait for the result of the web query
			yield return webresult;
			
			string[] records = webresult.text.Split('%');

			foreach (string record in records){
				try{
					//Debug.Log(record);
					string[] fields = record.Split('|');

					//outputText.text += fields[0] + " "+fields[1] +"\n";
					//make sure you don't sync the current player self
					//fields[6] red
					//fields[7] green
					//fields[8] blue
					
					if (int.Parse(fields[0]) != currentPlayer.GetComponent<playerController>().playerID)
					{
						canAdd = true;

						foreach (GameObject p in currentPlayers)
						{
							if (p.GetComponent<nonplayerController>().playerID == int.Parse(fields[0]))
							{
								canAdd = false;
							}
						}
						
						//add the player to the list
						//check the ids, filter the IDs of the players who are already in the game
						
						
						if (canAdd)
						{
							//instantiate a box at each position
							GameObject playerBox = Instantiate(player,
											new Vector3(float.Parse(fields[4]),float.Parse(fields[5])),
											Quaternion.identity);
							
							playerBox.name = fields[1];

							//we no longer want to set the colour to random
							//playerBox.GetComponent<SpriteRenderer>().color = Random.ColorHSV();
							float red = float.Parse(fields[6]);
							float green = float.Parse(fields[7]);
							float blue = float.Parse(fields[8]);

							//restore the colour from the server without transparency
							playerBox.GetComponent<SpriteRenderer>().color = new Color(red,green,blue,1);

							//set the name of each player box
							playerBox.GetComponentInChildren<TextMesh>().text = playerBox.name;

							playerBox.AddComponent<nonplayerController>();

							//get the ID from the database and add it to the script inside playerbox
							playerBox.GetComponent<nonplayerController>().playerID = int.Parse(fields[0]);

							int playeridtoadd = int.Parse(fields[0]);

							currentPlayers.Add(playerBox);
						}
					}
					
				}catch(System.Exception e){
					Debug.Log(e);
				}
			}
			//run the join game script AFTER I have loaded all the players on the web server
			if (!running){
				yield return joinGameOnServer();
				break;
			} else {
				//sync users every 2 seconds
				Debug.Log("Synced users again"+Time.time);
				yield return new WaitForSeconds(2f);
			}
		}
	}



	
	// Update is called once per frame
	void Update () {
		
	}
}
