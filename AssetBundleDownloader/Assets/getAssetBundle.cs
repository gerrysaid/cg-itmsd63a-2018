﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class getAssetBundle : MonoBehaviour {


    private AssetBundleCreateRequest bundleRequest;
    private UnityWebRequest request1,request2;

    // Use this for initialization
    void Start() {

        
        StartCoroutine(GetAssetBundle1());
    }

    public Sprite[] pictures;
    public GameObject[] objects;



    IEnumerator GetAssetBundle1()
    {

        request1 = UnityWebRequestAssetBundle.GetAssetBundle("http://localhost:8080/assetbundleapp/assetbundle2");
        yield return request1.SendWebRequest();

        if (request1.isNetworkError || request1.isHttpError)
        {
            Debug.Log(request1.error);
        }
        else
        {
            AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(request1);

            foreach (string name in bundle.GetAllAssetNames())
            {
                Debug.Log(name);
                
            }

            pictures = bundle.LoadAllAssets<Sprite>();
            objects = bundle.LoadAllAssets<GameObject>();

            //pictures = imageAssetBundle.allAssets as Sprite[];


            //AssetBundleRequest prefabAssetBundle = bundle.LoadAllAssetsAsync<GameObject>();
            //objects = prefabAssetBundle.allAssets as GameObject[];
            Instantiate(objects[0], new Vector3(0, 0), Quaternion.identity);

        }

        StartCoroutine(GetAssetBundle2());
      



        /*
        UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle("http://localhost:8080/assetbundleapp/test");
        yield return www.SendWebRequest();
 
        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
        }
        else {
            AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(www);
            // load asset asynchronously
            AssetBundleRequest request = bundle.LoadAssetAsync("mybundlescene", typeof(Scene));
            // Wait for request to complete
            yield return request;


            Scene myscene = request.asset as Scene;

            //Instantiate(gObject, new Vector3(0, 0), Quaternion.identity);
			
        }
        */
    }



    IEnumerator GetAssetBundle2() {

        Debug.Log("hello");
        request2 = UnityWebRequestAssetBundle.GetAssetBundle("http://localhost:8080/assetbundleapp/assetbundle1");
        yield return request2.SendWebRequest();

        if (request2.isNetworkError || request2.isHttpError)
        {
            Debug.Log(request2.error);
        }
        else
        {
            AssetBundle bundle2 = DownloadHandlerAssetBundle.GetContent(request2);
            
            foreach (string name in bundle2.GetAllScenePaths())
            {
                Debug.Log(name);
            }

        }


        /*
        string sceneURL = "http://localhost:8080/assetbundleapp/assetbundle1";
        WWW bundleWWW = WWW.LoadFromCacheOrDownload(sceneURL, 1);
        yield return bundleWWW;
        AssetBundle assetBundle = bundleWWW.assetBundle;

        if (assetBundle.isStreamedSceneAssetBundle)
        {
            string[] scenePaths = assetBundle.GetAllScenePaths();
            string sceneName = System.IO.Path.GetFileNameWithoutExtension(scenePaths[0]);
            SceneManager.LoadScene(sceneName);
        }

       
        UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle("http://localhost:8080/assetbundleapp/test");
        yield return www.SendWebRequest();
 
        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
        }
        else {
            AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(www);
            // load asset asynchronously
            AssetBundleRequest request = bundle.LoadAssetAsync("mybundlescene", typeof(Scene));
            // Wait for request to complete
            yield return request;


            Scene myscene = request.asset as Scene;

            //Instantiate(gObject, new Vector3(0, 0), Quaternion.identity);
			
        }
        */
    }
}
