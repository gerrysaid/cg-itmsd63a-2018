<?php 
//connect to the database at the top of the page as code is processed from top to bottom.
error_reporting(0);
$db = new mysqli('localhost','root','','example1',3307); 
//bring all players
if (isset($_GET) && ($_GET['operation']=="select"))
{
	//get the values for unity
	$sqlstatement = "select id,name,surname,age,xpos,ypos,red,green,blue from names";

	//this will get me the list of names in the database
	$names = $db->query($sqlstatement);

	while($row = $names->fetch_assoc())
	{
		echo "%".$row["id"]."|".
		$row["name"]."|".
		$row["surname"]."|".
		$row["age"]."|".
		$row["xpos"]."|".		
		$row["ypos"]."|".
		$row["red"]."|".
		$row["green"]."|".
		$row["blue"]."|";
	}
}
//new player joins game
if (isset($_GET) && ($_GET['operation']=="join")){
	//check the count of users, if the count is less than 5 you may insert
	//a new user, if not, return an error code to unity to stop the new player from joining
	//-----------check player count---------------
	//this SQL statement checks the number of players in the server
	$numberofplayers = 0;
	$sqlstatement = "select count(*) as numofplayers from names";
	$playercounts = $db->query($sqlstatement);
	while($row = $playercounts->fetch_assoc())
	{
		$numberofplayers = $row["numofplayers"];
	}

	if ($numberofplayers<5){
		//-----------if player count < 5 -------------
		$nickname = mysqli_real_escape_string($db,$_GET['nickname']);
		$surname = 'player';
		$xvalue = 0;
		$yvalue = 0;
		//-------------------------------
		//read the colour variables here
		$red = mysqli_real_escape_string($db,$_GET['r']);
		$green = mysqli_real_escape_string($db,$_GET['g']);
		$blue = mysqli_real_escape_string($db,$_GET['b']);

		//------------------------------
		//insert the values accordingly
		$sqlstatement = "insert into names(name,surname,age,xpos,ypos,red,green,blue)
		values ('$nickname','$surname',0,$xvalue,$yvalue,$red,$green,$blue)";

		if ($db->query($sqlstatement))
		{
			//return the ID of the newly created user to Unity
			echo "OK|".$db->insert_id."|";
		}
		else {
			echo $sqlstatement;
			echo $db->error();
		}
	}else {
		echo "NOK|Too many players!";
	}

}
//player decides to leave game
if (isset($_GET) && ($_GET['operation']=="leave")){
	$playerid = mysqli_real_escape_string($db,$_GET['playerid']);

	$sqlstatement = "delete from names where id = $playerid";

	if ($db->query($sqlstatement))
	{
		echo "OK|Deleted|";
	}
	else {
		echo $sqlstatement;
		echo $db->error();
	}

}
//player moves while playing, updating position on server
if (isset($_GET) && ($_GET['operation']=="move")){
	$playerid = mysqli_real_escape_string($db,$_GET['playerid']);
	$xvalue = mysqli_real_escape_string($db,$_GET['xpos']);
	$yvalue = mysqli_real_escape_string($db,$_GET['ypos']);

	$sqlstatement = "update names set xpos=$xvalue,ypos=$yvalue where id=$playerid";

	if ($db->query($sqlstatement))
	{
		echo "OK|Moved|";
	}
	else {
		echo $sqlstatement;
		echo $db->error();
	}

}
if (isset($_GET) && ($_GET['operation']=="getposition")){
	$playerid = mysqli_real_escape_string($db,$_GET['playerid']);
	$sqlstatement = "select xpos,ypos from names where id=$playerid";
	$coordinates = $db->query($sqlstatement);
	while($row = $coordinates->fetch_assoc())
	{
		echo "%".$row["xpos"]."|".$row["ypos"]."|";
	}
}
else {
?>

<html>
	<head>
		<title>Hello</title>
	</head>
	<body>
		<p>
		Hi my name is Gerry, what is your name please? 
		</p>
		<form method="get" action="index.php">
			<input type="hidden" name="operation" value="insert">
			Name: <input type="text" name="inputName" /><br/>
			Surname: <input type="text" name="inputSurname" /><br/>
			Age: <input type="text" name="inputAge" /><br/>
			X value: <input type="text" name="inputXValue" /><br/>
			Y value: <input type="text" name="inputYValue" /><br/>
			<input type="submit" value="Insert"/>
		</form>
		<?php
			//if submit is pressed, show the submitted value
			if (isset($_GET) && ($_GET['operation']=="insert"))
			{
				$name = mysqli_real_escape_string($db,$_GET['inputName']);
				$surname = mysqli_real_escape_string($db,$_GET['inputSurname']);
				$age = mysqli_real_escape_string($db,$_GET['inputAge']);
				$xvalue = mysqli_real_escape_string($db,$_GET['inputXValue']);
				$yvalue = mysqli_real_escape_string($db,$_GET['inputYValue']);
				
				$insertstatement = "insert into names(name,surname,age,xpos,ypos) values (
				'$name',
				'$surname',
				$age,
				$xvalue,
				$yvalue);";
				
				//for testing (to see that the query is correct)
				echo $insertstatement;
				
				$db->query($insertstatement);
				
			}
			//delete operation
			if (isset($_GET) && ($_GET['operation']=="delete"))
			{
				echo "ID to delete is: ".$_GET['id']."<br/>";
				
				$idtodelete = mysqli_real_escape_string($db,$_GET['id']);
				
				$deletestatement = "delete from names where id = $idtodelete";
				
				echo $deletestatement;
				//this will finally delete the record from the database
				$db->query($deletestatement);
				
			}
			if (isset($_GET) && ($_GET['operation']=="updatestage1"))
			{
				$idtoupdate = mysqli_real_escape_string($db,$_GET['id']);
				
				$sqlstatement = "select id,name,surname,age,xpos,ypos from names where id=$idtoupdate";
				
				echo $sqlstatement;
				
				$recordtoupdate = $db->query($sqlstatement);
				
				if ($row = $recordtoupdate->fetch_assoc())
				{
			?>	
				<form method="get" action="index.php">
					<input type="hidden" name="operation" value="updatestage2">
					<input type="hidden" name="id" value="<?php echo $row['id']; ?>">
					Name: <input type="text" name="inputName" value="<?php echo $row['name']; ?>" /><br/>
					Surname: <input type="text" name="inputSurname" value="<?php echo $row['surname']; ?>" /><br/>
					Age: <input type="text" name="inputAge" value="<?php echo $row['age']; ?>" /><br/>
					X value: <input type="text" name="inputXValue" value="<?php echo $row['xpos']; ?>" /><br/>
					Y value: <input type="text" name="inputYValue" value="<?php echo $row['ypos']; ?>" /><br/>
					<input type="submit" value="Update"/>
				</form>	
			<?php
				}
			}
			//user has pressed update on the update form field
			if (isset($_GET) && ($_GET['operation']=="updatestage2"))
			{	
				$idtoupdate = mysqli_real_escape_string($db,$_GET['id']);
				$newname = mysqli_real_escape_string($db,$_GET['inputName']);
				$newsurname = mysqli_real_escape_string($db,$_GET['inputSurname']);
				$newage = mysqli_real_escape_string($db,$_GET['inputAge']);
				$newxvalue = mysqli_real_escape_string($db,$_GET['inputXValue']);
				$newyvalue = mysqli_real_escape_string($db,$_GET['inputYValue']);

				$sqlstatement = "update names set name='$newname',
									surname='$newsurname',
									age=$newage,
									xpos=$newxvalue,
									ypos=$newyvalue
								where id=$idtoupdate";
				
				echo $sqlstatement;

				$db->query($sqlstatement) or die(mysqli_error($db));
			}
			
			
			
			
		?>
		<h2>List of names from database</h2>
		
		<p>The list of names below is being selected from the mysql database</p>
		
		<?php
		//reference to the database that we are using in mysql
		
		
		//sql to get the names in my table
		$sqlstatement = "select id,name,surname,age from names";
		
		//this will get me the list of names in the database
		$names = $db->query($sqlstatement);
		

	
		while($row = $names->fetch_assoc())
		{
			?>
			<hr/>
			<p>Name: <?php echo $row['name']; ?></p>
			<p>Surname: <?php echo $row['surname']; ?></p>
			<p>Age: <?php echo $row['age']; ?></p>
			<p><a href="index.php?operation=delete&id=<?php echo $row['id']; ?>" onclick="return confirm('Sure?');">Delete</a></p>
			<p><a href="index.php?operation=updatestage1&id=<?php echo $row['id']; ?>">Update</a></p>
			
			<hr/>
		<?php
		}	
		?>
		
		
	</body>
</html>
	<?php } ?>